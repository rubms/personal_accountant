'use strict';

/**
 * @ngdoc function
 * @name webClientScratchApp.controller:ExpenseslistcontrollerCtrl
 * @description
 * # ExpenseslistcontrollerCtrl
 * Controller of the webClientScratchApp
 */
angular.module('webClientScratchApp')
  .controller('ExpenseslistcontrollerCtrl', ['$scope', '$http',
      function($scope, $http) {
          $http.get('fake_data/expenses.json').success(function(data) {
            $scope.expenses = data.expenses;
          });  
      }]);
