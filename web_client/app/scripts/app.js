'use strict';

/**
 * @ngdoc overview
 * @name webClientScratchApp
 * @description
 * # webClientScratchApp
 *
 * Main module of the application.
 */
angular
  .module('webClientScratchApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/expenses', {
        templateUrl: 'views/expense-list.html',
        controller: 'ExpenseslistcontrollerCtrl'
      })
      .otherwise({
        redirectTo: '/expenses'
      });
  });
