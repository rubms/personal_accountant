from django.shortcuts import render
from django.http import HttpResponse
import datetime
from errorcodes import ErrorCodes
from personalaccountant.models import Expense, Wallet, Estimate, Category
import json
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def index(request):
	return HttpResponse("Hola tron")

def getExpenses(request, startDate, endDate, walletId):
	if not request.user.is_authenticated():
		context = {'error_message': 'User not logged', 'error_code' : ErrorCodes.NO_USER_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	if request.method != 'GET':
		context = {'error_message': 'Method not supported', 'error_code' : ErrorCodes.METHOD_NOT_SUPPORTED}
		return render(request, 'personalaccountant/error.json', context)
		
	expenseList = Expense.objects.filter(wallet__user__id=request.user.id, wallet__id=walletId, date__gte=datetime.datetime.utcfromtimestamp(int(startDate)), date__lte=datetime.datetime.utcfromtimestamp(int(endDate))) 
	wallet = Wallet.objects.get(id=walletId)
	context = {'expense_list' : expenseList, 'wallet' : wallet }
	return render(request, 'personalaccountant/expense_list.json', context)
		
def createExpense(request):
	if not request.user.is_authenticated():
		context = {'error_message': 'User not logged', 'error_code' : ErrorCodes.NO_USER_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	if request.method != 'POST':
		context = {'error_message': 'Method not supported', 'error_code' : ErrorCodes.METHOD_NOT_SUPPORTED}
		return render(request, 'personalaccountant/error.json', context)

	requestExpense = json.loads(request.body)
	Expense.objects.create(date=datetime.datetime.utcfromtimestamp(requestExpense["date"]), description=requestExpense["description"], amount=requestExpense["amount"], category=Category.objects.get(pk=requestExpense["category"]), wallet=Wallet.objects.get(pk=requestExpense["wallet"]))
	return render(request, 'personalaccountant/ok.json')
	
def getWallets(request):
	if not request.user.is_authenticated():
		context = {'error_message': 'User not logged', 'error_code' : ErrorCodes.NO_USER_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	walletList = Wallet.objects.filter(user__id=request.user.id)
	context = {'wallet_list' : walletList}
	return render(request, 'personalaccountant/wallet_list.json', context)

def createWallet(request):
	if not request.user.is_authenticated():
		context = {'error_message': 'User not logged', 'error_code' : ErrorCodes.NO_USER_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	if request.method != 'POST':
		context = {'error_message': 'Method not supported', 'error_code' : ErrorCodes.METHOD_NOT_SUPPORTED}
		return render(request, 'personalaccountant/error.json', context)

	requestWallet = json.loads(request.body)
	Wallet.objects.create(name=requestWallet["name"], user=request.user)
	return render(request, 'personalaccountant/ok.json')

def getEstimates(request, date, walletId):
	if not request.user.is_authenticated():
		context = {'error_message': 'User not logged', 'error_code' : ErrorCodes.NO_USER_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	if request.method != 'GET':
		context = {'error_message': 'Method not supported', 'error_code' : ErrorCodes.METHOD_NOT_SUPPORTED}
		return render(request, 'personalaccountant/error.json', context)
	
	specifiedDate = datetime.datetime.utcfromtimestamp(int(date))	
	estimateList = Estimate.objects.filter(wallet__user__id=request.user.id, wallet__id=walletId, validityStart__lte=specifiedDate, validityEnd__gte=specifiedDate) 
	context = {'estimate_list' : estimateList}
	return render(request, 'personalaccountant/estimate_list.json', context)

def createEstimate(request):
	if not request.user.is_authenticated():
		context = {'error_message': 'User not logged', 'error_code' : ErrorCodes.NO_USER_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	if request.method != 'POST':
		context = {'error_message': 'Method not supported', 'error_code' : ErrorCodes.METHOD_NOT_SUPPORTED}
		return render(request, 'personalaccountant/error.json', context)

	requestEstimate = json.loads(request.body)
	Estimate.objects.create(validityStart=datetime.datetime.utcfromtimestamp(requestEstimate["validityStart"]), validityEnd=datetime.datetime.utcfromtimestamp(requestEstimate["validityEnd"]), description=requestEstimate["description"], amount=requestEstimate["amount"], category=Category.objects.get(pk=requestEstimate["category"]), wallet=Wallet.objects.get(pk=requestEstimate["wallet"]))
	return render(request, 'personalaccountant/ok.json')
	
def createCategory(request):
	if not request.user.is_authenticated():
		context = {'error_message': 'User not logged', 'error_code' : ErrorCodes.NO_USER_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	if request.method != 'POST':
		context = {'error_message': 'Method not supported', 'error_code' : ErrorCodes.METHOD_NOT_SUPPORTED}
		return render(request, 'personalaccountant/error.json', context)

	requestCategory = json.loads(request.body)
	Category.objects.create(name=requestCategory["name"], user=request.user)
	return render(request, 'personalaccountant/ok.json')
	
def getCategories(request):
	if not request.user.is_authenticated():
		context = {'error_message': 'User not logged', 'error_code' : ErrorCodes.NO_USER_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	if request.method != 'GET':
		context = {'error_message': 'Method not supported', 'error_code' : ErrorCodes.METHOD_NOT_SUPPORTED}
		return render(request, 'personalaccountant/error.json', context)

	categoryList = Category.objects.filter(user__id=request.user.id)
	context = {'category_list' : categoryList}
	return render(request, 'personalaccountant/category_list.json', context)
	
def loginView(request):
	if request.user.is_authenticated():
		context = {'error_message': 'User already logged', 'error_code' : ErrorCodes.USER_ALREADY_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	if request.method != 'POST':
		context = {'error_message': 'Method not supported', 'error_code' : ErrorCodes.METHOD_NOT_SUPPORTED}
		return render(request, 'personalaccountant/error.json', context)
		
	requestInfo = json.loads(request.body)
	user = authenticate(username=requestInfo["username"], password=requestInfo["password"])
	if user is not None:
		login(request, user)
		return render(request, 'personalaccountant/ok.json')
	else:
		context = {'error_message': 'Wrong credentials', 'error_code' : ErrorCodes.WRONG_CREDENTIALS}
		return render(request, 'personalaccountant/error.json', context)
		
def logoutView(request):
	if not request.user.is_authenticated():
		context = {'error_message': 'User not logged', 'error_code' : ErrorCodes.NO_USER_LOGGED_IN}
		return render(request, 'personalaccountant/error.json', context)

	if request.method != 'POST':
		context = {'error_message': 'Method not supported', 'error_code' : ErrorCodes.METHOD_NOT_SUPPORTED}
		return render(request, 'personalaccountant/error.json', context)
		
	logout(request)
	return render(request, 'personalaccountant/ok.json')
