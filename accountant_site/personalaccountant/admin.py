from django.contrib import admin
from personalaccountant.models import Wallet, Category, Expense, Estimate

# Register your models here.
admin.site.register(Wallet)
admin.site.register(Category)
admin.site.register(Expense)
admin.site.register(Estimate)


