from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Wallet(models.Model):
	name = models.CharField(max_length=50)
	user = models.ForeignKey(User)
	
	def __unicode__(self):
		return self.name
	
class Category(models.Model):
	name = models.CharField(max_length=50)
	user = models.ForeignKey(User)

	def __unicode__(self):
		return self.name

class Expense(models.Model):
	date = models.DateTimeField('transaction date')
	description = models.CharField(max_length=200)
	amount = models.DecimalField(max_digits=18, decimal_places=2)
	category = models.ForeignKey(Category)
	wallet = models.ForeignKey(Wallet)

	def __unicode__(self):
		return self.description

class Estimate(models.Model):
	validityStart = models.DateTimeField('validity start')
	validityEnd = models.DateTimeField('validity end')
	description = models.CharField(max_length=200)
	amount = models.DecimalField(max_digits=18, decimal_places=2)
	category = models.ForeignKey(Category)
	wallet = models.ForeignKey(Wallet)

	def __unicode__(self):
		return self.description

