from django.conf.urls import patterns, url

from personalaccountant import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^expenses/(?P<startDate>\d+)/(?P<endDate>\d+)/(?P<walletId>\d+)/$', views.getExpenses, name='getExpenses'),
    url(r'^create_expense/$', views.createExpense, name='createExpense'),
    
    url(r'^wallets/$', views.getWallets, name='getWallets'),
    url(r'^create_wallet/$', views.createWallet, name='createWallet'),
    
    url(r'^estimates/(?P<date>\d+)/(?P<walletId>\d+)/$', views.getEstimates, name='getEstimates'),
    url(r'^create_estimate/$', views.createEstimate, name='createEstimate'),
    
    url(r'^categories/$', views.getCategories, name='getCategories'),
    url(r'^create_category/$', views.createCategory, name='createCategory'),
    
    url(r'^login/$', views.loginView, name='loginView'),
    url(r'^logout/$', views.logoutView, name='logoutView'),
)
