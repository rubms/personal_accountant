from django.test import TestCase
from personalaccountant.models import Expense, Wallet, Category, Estimate
import datetime
from django.contrib.auth.models import User
from errorcodes import ErrorCodes
import json

# Create your tests here.
class PersonalAccountant(TestCase):
	def setUp(self):
		test_user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
		test_wallet = Wallet.objects.create(name="cashjohn", user=test_user)
		test_category = Category.objects.create(name="foodjohn", user=test_user)
		Estimate.objects.create(validityStart=datetime.datetime.utcfromtimestamp(1000), validityEnd=datetime.datetime.utcfromtimestamp(2000), description="cashjohnEstimate1", amount=40.0, category=test_category, wallet=test_wallet)
		Estimate.objects.create(validityStart=datetime.datetime.utcfromtimestamp(1000), validityEnd=datetime.datetime.utcfromtimestamp(2000), description="cashjohnEstimate2", amount=40.0, category=test_category, wallet=test_wallet)
		Estimate.objects.create(validityStart=datetime.datetime.utcfromtimestamp(1000), validityEnd=datetime.datetime.utcfromtimestamp(2000), description="cashjohnEstimate3", amount=40.0, category=test_category, wallet=test_wallet)
		Estimate.objects.create(validityStart=datetime.datetime.utcfromtimestamp(900), validityEnd=datetime.datetime.utcfromtimestamp(1000), description="cashjohnEstimate4", amount=40.0, category=test_category, wallet=test_wallet)
		Estimate.objects.create(validityStart=datetime.datetime.utcfromtimestamp(2000), validityEnd=datetime.datetime.utcfromtimestamp(3000), description="cashjohnEstimate5", amount=40.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(999), description="cashjohnGroceries1", amount=10.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(1001), description="cashjohnGroceries2", amount=10.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(1002), description="cashjohnGroceries3", amount=10.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(2002), description="cashjohnGroceries4", amount=10.0, category=test_category, wallet=test_wallet)

		test_wallet = Wallet.objects.create(name="cash2john", user=test_user)
		test_category = Category.objects.create(name="foodjohn2", user=test_user)
		Estimate.objects.create(validityStart=datetime.datetime.utcfromtimestamp(1000), validityEnd=datetime.datetime.utcfromtimestamp(2000), description="cash2johnEstimate", amount=40.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(999), description="cash2johnGroceries1", amount=10.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(1001), description="cash2johnGroceries2", amount=10.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(1002), description="cash2johnGroceries3", amount=10.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(2002), description="cash2johnGroceries4", amount=10.0, category=test_category, wallet=test_wallet)

		test_user = User.objects.create_user('paul', 'mccartney@thebeatles.com', 'paulpassword2')
		test_wallet = Wallet.objects.create(name="cashpaul", user=test_user)
		test_category = Category.objects.create(name="foodpaul", user=test_user)
		Estimate.objects.create(validityStart=datetime.datetime.utcfromtimestamp(1000), validityEnd=datetime.datetime.utcfromtimestamp(2000), description="cashpaulEstimate", amount=40.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(999), description="cashpaulGroceries1", amount=10.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(1001), description="cashpaulGroceries2", amount=10.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(1002), description="cashpaulGroceries3", amount=10.0, category=test_category, wallet=test_wallet)
		Expense.objects.create(date=datetime.datetime.utcfromtimestamp(2002), description="cashpaulGroceries4", amount=10.0, category=test_category, wallet=test_wallet)


	def test_get_expenses(self):
		self.client.login(username='john', password='johnpassword')
		response = self.client.get('/personalaccountant/expenses/1000/2000/1/')
		self.assertEqual(response.status_code, 200)
		print response.content

		responseObject = json.loads(response.content)
		expensesArray = responseObject["expenses"]
		self.assertEqual(2, len(expensesArray))
		self.assertEqual('cashjohnGroceries2', expensesArray[0]["description"])
		self.assertEqual('cashjohnGroceries3', expensesArray[1]["description"])
		self.assertEqual(10, expensesArray[0]["amount"])
		self.assertEqual(10, expensesArray[1]["amount"])
		self.assertEqual("1970-01-01T00:16:41", expensesArray[0]["date"])
		self.assertEqual("1970-01-01T00:16:42", expensesArray[1]["date"])
		self.assertEqual(1, expensesArray[0]["category"])
		self.assertEqual(1, expensesArray[1]["category"])
		
	def test_unlogged_get_expenses(self):
		response = self.client.get('/personalaccountant/expenses/1000/2000/1/')
		self.assertEqual(response.status_code, 200)
		
		self.assertIn('error', response.content)
		self.assertIn('"error_code" : "' + str(ErrorCodes.NO_USER_LOGGED_IN) + '"', response.content)
		
		
	def test_get_wallets(self):
		self.client.login(username='john', password='johnpassword')
		response = self.client.get('/personalaccountant/wallets/')
		self.assertEqual(response.status_code, 200)
		print response.content

		responseObject = json.loads(response.content)
		categoriesArray = responseObject["wallets"]
		self.assertEqual(2, len(categoriesArray))
		self.assertEqual('cashjohn', categoriesArray[0]["name"])
		self.assertEqual('cash2john', categoriesArray[1]["name"])
				
								
	def test_create_wallet(self):
		self.client.login(username='john', password='johnpassword')

		requestString = '{"name":"BankAccount2"}'
		response = self.client.post('/personalaccountant/create_wallet/', content_type='application/json', data=requestString)

		self.assertEqual(response.status_code, 200)
		readWallet = Wallet.objects.get(name="BankAccount2", user__username='john')
		self.assertNotEqual(None, readWallet)
				
	def test_get_estimates(self):
		self.client.login(username='john', password='johnpassword')
		response = self.client.get('/personalaccountant/estimates/1500/1/')
		self.assertEqual(response.status_code, 200)
		print response.content
		
		responseObject = json.loads(response.content)
		estimatesArray = responseObject["estimates"]
		self.assertEqual(3, len(estimatesArray))
		self.assertEqual('cashjohnEstimate1', estimatesArray[0]["description"])
		self.assertEqual('cashjohnEstimate2', estimatesArray[1]["description"])
		self.assertEqual('cashjohnEstimate3', estimatesArray[2]["description"])
		self.assertEqual(40, estimatesArray[0]["amount"])
		self.assertEqual("1970-01-01T00:16:40", estimatesArray[0]["validityStart"])
		self.assertEqual("1970-01-01T00:33:20", estimatesArray[0]["validityEnd"])
		self.assertEqual(1, estimatesArray[0]["category"])
		self.assertEqual(1, estimatesArray[1]["category"])		
		self.assertEqual(1, estimatesArray[2]["category"])		

		
	def test_unlogged_get_estimates(self):
		response = self.client.get('/personalaccountant/expenses/1000/2000/1/')
		self.assertEqual(response.status_code, 200)

		self.assertIn('error', response.content)
		self.assertIn('"error_code" : "' + str(ErrorCodes.NO_USER_LOGGED_IN) + '"', response.content)

	def test_create_expense(self):
		self.client.login(username='john', password='johnpassword')

		testCategory = Category.objects.all()[0]
		testWallet = Wallet.objects.filter(user__username='john')[0]
		requestString = '{{"date":1850, "description":"Gas refill", "amount":80.0, "category":{0}, "wallet":{1} }}'.format(testCategory.id, testWallet.id)
		response = self.client.post('/personalaccountant/create_expense/', content_type='application/json', data=requestString)

		self.assertEqual(response.status_code, 200)
		readExpense = Expense.objects.get(description="Gas refill", amount=80.0)
		self.assertEqual(datetime.datetime.utcfromtimestamp(1850), readExpense.date)
		self.assertEqual("Gas refill", readExpense.description)
		self.assertEqual(80.0, readExpense.amount)
		self.assertEqual(testCategory, readExpense.category)
		self.assertEqual(testWallet, readExpense.wallet)

	def test_unlogged_create_expense(self):
		response = self.client.post('/personalaccountant/create_expense/')
		self.assertEqual(response.status_code, 200)

		self.assertIn('error', response.content)
		self.assertIn('"error_code" : "' + str(ErrorCodes.NO_USER_LOGGED_IN) + '"', response.content)
		
	def test_create_category(self):
		self.client.login(username='john', password='johnpassword')

		requestString = '{"name":"Transportation"}'
		response = self.client.post('/personalaccountant/create_category/', content_type='application/json', data=requestString)
		
		self.assertEqual(response.status_code, 200)
		readCategory = Category.objects.get(name="Transportation", user__username='john')
		self.assertNotEqual(None, readCategory)

	def test_get_categories(self):
		self.client.login(username='john', password='johnpassword')
		response = self.client.get('/personalaccountant/categories/')
		self.assertEqual(response.status_code, 200)
		print response.content
		
		responseObject = json.loads(response.content)
		categoriesArray = responseObject["categories"]
		self.assertEqual(2, len(categoriesArray))
		self.assertEqual('foodjohn', categoriesArray[0]["name"])
		self.assertEqual('foodjohn2', categoriesArray[1]["name"])
		
	def test_login(self):
		requestString = '{"username":"john", "password":"johnpassword"}'
		response = self.client.post('/personalaccountant/login/', content_type='application/json', data=requestString)
		
		responseObject = json.loads(response.content)
		self.assertEqual('OK', responseObject["result"])
		
		self.client.login(username='john', password='johnpassword')
		response = self.client.get('/personalaccountant/categories/')
		responseObject = json.loads(response.content)
		categoriesArray = responseObject["categories"]
		self.assertEqual(2, len(categoriesArray))
		self.assertEqual('foodjohn', categoriesArray[0]["name"])
		self.assertEqual('foodjohn2', categoriesArray[1]["name"])
		self.assertFalse('error' in responseObject)

	def test_logout(self):
		self.client.login(username='john', password='johnpassword')
		response = self.client.post('/personalaccountant/logout/', content_type='application/json', data='')
		
		responseObject = json.loads(response.content)
		self.assertEqual('OK', responseObject["result"])

		response = self.client.get('/personalaccountant/categories/')
		responseObject = json.loads(response.content)
		self.assertTrue('error' in responseObject)

	def test_create_estimate(self):
		self.client.login(username='john', password='johnpassword')

		testCategory = Category.objects.all()[0]
		testWallet = Wallet.objects.filter(user__username='john')[0]
		requestString = '{{"validityStart":1500, "validityEnd":2000, "description":"Transportation", "amount":80.0, "category":{0}, "wallet":{1} }}'.format(testCategory.id, testWallet.id)
		response = self.client.post('/personalaccountant/create_estimate/', content_type='application/json', data=requestString)
		self.assertEqual(response.status_code, 200)

		responseObject = json.loads(response.content)
		self.assertEqual('OK', responseObject["result"])

		readExpense = Estimate.objects.get(description="Transportation", amount=80.0)
		self.assertEqual(datetime.datetime.utcfromtimestamp(1500), readExpense.validityStart)
		self.assertEqual(datetime.datetime.utcfromtimestamp(2000), readExpense.validityEnd)
		self.assertEqual("Transportation", readExpense.description)
		self.assertEqual(80.0, readExpense.amount)
		self.assertEqual(testCategory, readExpense.category)
		self.assertEqual(testWallet, readExpense.wallet)

	def test_unlogged_create_estimate(self):
		response = self.client.post('/personalaccountant/create_estimate/')
		self.assertEqual(response.status_code, 200)

		responseObject = json.loads(response.content)
		self.assertTrue('error' in responseObject)
		self.assertEqual(ErrorCodes.NO_USER_LOGGED_IN, int(responseObject["error"]["error_code"]))
