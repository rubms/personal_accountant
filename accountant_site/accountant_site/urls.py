from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'accountant_site.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^personalaccountant/', include('personalaccountant.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
